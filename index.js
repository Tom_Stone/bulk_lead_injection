var InsertLeads = require("./InsertLeads");
var constants = require('./Constants');

PushLeads()

async function PushLeads(){
    for (let i = 0; i<constants.ItterationCount; i++)
    {
        await InsertLeads.Push(constants.accountid, constants.formid, constants.email, constants.username).then(function(){
            let percentage = Math.round((i/constants.ItterationCount)*100).toString() + "%"
            process.stdout.write(`\rLoading: ${percentage}`);
                if (i+1 ==constants.ItterationCount)
                {
                    process.stdout.write(`\rDone: 100%`);
                }
        })
        .catch(function(){
            process.stdout.write(`\r ERROR`);
            i = constants.ItterationCount
        })
    }
    
}