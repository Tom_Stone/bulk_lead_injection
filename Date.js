exports.Now = function(){
    var datetime = new Date();
    return datetime;
}

exports.Plus5Days = function(){
    var datetime = new Date();
    datetime.setDate(datetime.getDate() + 5);
    return datetime;
}

exports.LeadUTC= function(){
    var datetime =new Date().toISOString().
  replace(/T/, ' ').
  replace(/\..+/, '')
  return datetime;
}

exports.LeadUTCLong= function(){
    var datetime =new Date().toISOString().
  replace(/T/, ' ').
  replace(/Z/, '')
  return datetime;
}

exports.ZauthTime= function(){
  var datetime =new Date().toISOString().
replace(/T/, '').
replace(/\..+/, '').
replace(/\-/g, '').
replace(/\:/g, '')

datetime = datetime.substring(0, datetime.length - 2)

return datetime;
}